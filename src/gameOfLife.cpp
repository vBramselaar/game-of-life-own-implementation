#include "gameOfLife.h"

GameOfLife::GameOfLife(int width, int height)
{
	grid = grid_t(height, row_t(width));
	
    playWidth = width;
    playHeight = height;
}

GameOfLife::~GameOfLife()
{
}

int8_t GameOfLife::getCell(size_t y, size_t x) const
{
    if (x < static_cast<size_t>(playWidth) && y < static_cast<size_t>(playHeight))
    {
		return grid.at(y).at(x);
    }
    return -1;
}

void GameOfLife::setCell(size_t y, size_t x, uint8_t value)
{
    if (x < static_cast<size_t>(playWidth) && y < static_cast<size_t>(playHeight))
    {
		grid.at(y).at(x) = value;
    }
}

int mod(int a, int b) {
    int c = a % b;
    return (c < 0) ? c + b : c;
}

void GameOfLife::evolveRow(const grid_t& grid, grid_t& newGrid, size_t row)
{
	for(int i = 0; i < playWidth; i++)
	{
		int neighbours = 0;

		size_t oneBack = mod(i-1, playWidth);
		size_t oneForward = mod(i+1, playWidth);
			
		//upper row
		neighbours += grid[mod((row-1), playHeight)][oneBack];
		neighbours += grid[mod((row-1), playHeight)][i];
		neighbours += grid[mod((row-1), playHeight)][oneForward];

		//middle row
		neighbours += grid[row][oneBack];
		neighbours += grid[row][oneForward];

		//down row
		neighbours += grid[mod((row+1), playHeight)][oneBack];
		neighbours += grid[mod((row+1), playHeight)][i];
		neighbours += grid[mod((row+1), playHeight)][oneForward];

		if (grid[row][i] == 1 && (neighbours == 3 || neighbours == 2))
		{
			newGrid[row][i] = 1;
		}
		else if (grid[row][i] == 0 && neighbours == 3)
		{
			newGrid[row][i] = 1;
		}
		else
		{
			newGrid[row][i] = 0;
		}
	}
}

int8_t GameOfLife::evolve()
{
	grid_t newGrid = grid_t(playHeight, row_t(playWidth));

#ifdef THREAD_MODE
	std::vector<std::thread> threads;
	threads.reserve(playHeight);
	for (int h = 0; h < playHeight; h++)
	{
		threads.emplace_back(std::thread(&GameOfLife::evolveRow, this, std::ref(grid), std::ref(newGrid), h));
	}

	for (std::thread& t : threads)
	{
		t.join();
	}
#else
	for(int h = 0; h < playHeight; h++)
	{
		evolveRow(grid, newGrid, h);
	}
#endif

	grid = newGrid;
	return 0;
}


