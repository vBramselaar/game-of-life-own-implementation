#include <iostream>
#include <chrono>
#include <thread>
#include <random>
#include <functional>
#include <sstream>

#include "gameOfLife.h"

#define block "\u2588"

void show(GameOfLife& gol)
{
	std::cout << "----------------------------------" << std::endl;
	std::stringstream blockStream;
	
	for (int h = 0; h < gol.getHeight(); h++)
	{
		for (int w = 0; w < gol.getWidth(); w++)
		{
			int8_t value = gol.getCell(h, w);
			if(value  == 1)
			{
				blockStream << block;
			}
			else
			{
				blockStream << " ";
			}
		}
		blockStream << std::endl;
	}

	std::cout << blockStream.rdbuf();
}

int main(int argc, char *argv[])
{
	if(argc != 4)
	{
		std::cout << "syntax: [width] [height] [percentage]" << std::endl;
		return -1;
	}

	int width = std::stoi(argv[1]);
	int height = std::stoi(argv[2]);
	GameOfLife gol(width, height);
	
	int percentage = std::stoi(argv[3]);
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine e(seed);
	std::uniform_int_distribution<> d(0, 99);
	auto rnd = std::bind(d, e);
	
	for(int i = 0; i < gol.getHeight(); i++)
	{
		for(int j = 0; j < gol.getWidth(); j++)
		{
			int value = rnd();
			if(value < percentage)
			{
				gol.setCell(i, j, 1);
			}
		}
	}

	int generation = 0;
	while(true)
	{
		auto start = std::chrono::high_resolution_clock::now();
		show(gol);
		gol.evolve();
		auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - start);
		std::cout << "Generation: " << generation << ", " << duration.count() << " ms"<< std::endl;		
		generation++;
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	}
	return 0;
}
