#ifndef GAMEOFLIFE_H
#define GAMEOFLIFE_H

#include <cstdint>
#include <vector>
#include <thread>

using row_t = std::vector<uint8_t>;
using grid_t = std::vector<row_t>;

class GameOfLife
{
public:
    GameOfLife(int width, int height);
    ~GameOfLife();

    int getWidth() const { return playWidth; }
    int getHeight() const { return playHeight; }
    
    int8_t getCell(size_t y, size_t x) const;
    void setCell(size_t y, size_t x, uint8_t value);
    int8_t evolve();

private:
	grid_t grid;
    int playWidth;
    int playHeight;
	
	void evolveRow(const grid_t& grid, grid_t& newGrid, size_t row);
};

#endif
