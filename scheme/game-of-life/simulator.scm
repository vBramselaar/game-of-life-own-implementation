
(define (make-world w h)
  (cons (make-grid w h #f) 0))

(define (evolve world)
  (let* ((grid (car world))
         (generation (cdr world))
         (new-grid (grid-copy grid)))
    (grid-for-each-point grid
                         (lambda (point)
                           (let ((x (car point))
                                 (y (cdr point)))
                             (if (alive? grid x y)
                                 (grid-set! new-grid x y #t)
                                 (grid-set! new-grid x y #f)))))
    (cons new-grid (+ generation 1))))

(define neighbours-offsets
  '((-1 . -1) (0 . -1) (1 . -1)
    (-1 .  0)          (1 .  0)
    (-1 .  1) (0 .  1) (1 .  1)))

(define (alive? grid x y)
  (let ((cell-lives (eqv? (grid-ref grid x y) #t))
        (count (neighbour-count grid x y)))
    (or (and cell-lives (or (= count 2) (= count 3)))
        (and (not cell-lives) (= count 3)))))

(define (neighbour-count grid x y)
  (let loop ((offsets neighbours-offsets)
             (count 0))
    (cond
     ((null? offsets)
      count)
     (else
      (let ((neighbour (grid-wrapped-ref grid (+ x (car (car offsets))) (+ y (cdr (car offsets))))))
        (if (eqv? neighbour #t)
            (loop (cdr offsets) (+ count 1))
            (loop (cdr offsets) count)))))))

(define (add-glider world x y)
  (let ((new-grid (grid-copy (car world))))
    (grid-set! new-grid (+ x 2) y #t)
    (grid-set! new-grid x (+ y 1) #t)
    (grid-set! new-grid (+ x 2) (+ y 1) #t)
    (grid-set! new-grid (+ x 1) (+ y 2) #t)
    (grid-set! new-grid (+ x 2) (+ y 2) #t)
    (cons new-grid (cdr world))))

(define (add-random-living world random-proc percentage)
  (cons (grid-map (car world)
                  (lambda (v)
                    (< (random-proc 99) percentage)))
        (cdr world)))
