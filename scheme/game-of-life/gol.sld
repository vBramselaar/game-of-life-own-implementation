(define-library (game-of-life gol)
  (import (scheme base)
          (scheme write)
          (scheme case-lambda))
  (export evolve make-world add-glider add-random-living
          visualize-in-terminal)
  (include "grid.scm")
  (include "simulator.scm")
  (include "visualizer.scm"))
