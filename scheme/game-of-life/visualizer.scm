
(define alive-symbol "█")
(define dead-symbol "_")

(define (print-grid grid)
  (for-each-in-vector grid
                      (lambda (row)
                        (for-each-in-vector row
                                            (lambda (cell)
                                              (if (eqv? cell #t)
                                                  (display alive-symbol)
                                                  (display dead-symbol))))
                        (newline))))

(define (visualize-in-terminal world)
  (display "===============================================") (newline)
  (print-grid (car world))
  (display "========== Generation ") (display (cdr world)) (display " ============") (newline)
  world)
