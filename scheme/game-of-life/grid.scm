(define make-grid
  (case-lambda
    ((w h) (make-vector h (make-vector w)))
    ((w h fill)
     (let ((grid (make-vector h)))
       (do ((i 0 (+ i 1)))
           ((>= i h) grid)
         (vector-set! grid i (make-vector w fill)))))))

(define (grid-width grid)
  (vector-length (vector-ref grid 0)))

(define (grid-height grid)
  (vector-length grid))

(define (grid-ref grid x y)
  (vector-ref (vector-ref grid y) x))

(define (grid-wrapped-ref grid x y)
  (let* ((wrapped-x (modulo x (grid-width grid)))
         (wrapped-y (modulo y (grid-height grid))))
    (grid-ref grid wrapped-x wrapped-y)))

(define (grid-set! grid x y value)
  (vector-set! (vector-ref grid y) x value))

(define (grid-point-set! grid point value)
  (vector-set! (vector-ref grid (cdr point)) (car point) value))

(define (grid-copy grid)
  (let* ((h (grid-height grid))
         (copy (make-vector h)))
    (do ((i 0 (+ i 1)))
        ((>= i h) copy)
      (vector-set! copy i (vector-copy (vector-ref grid i))))))

(define (grid-for-each-point grid proc)
  (do ((i 0 (+ i 1)))
      ((>= i (grid-height grid)))
    (do ((j 0 (+ j 1)))
        ((>= j (grid-width grid)))
      (proc (cons j i)))))

(define (grid-map grid proc)
  (let ((mapped-grid (grid-copy grid)))
    (grid-for-each-point grid
                         (lambda (point)
                           (grid-point-set! mapped-grid
                                            point
                                            (proc (grid-ref-point grid point)))))
    mapped-grid))

(define (grid-ref-point grid point)
  (grid-ref grid (car point) (cdr point)))

(define (for-each-in-vector vector proc)
  (unless (or (null? vector) (not (vector? vector)))
    (do ((i 0 (+ i 1)))
        ((>= i (vector-length vector)))
      (proc (vector-ref vector i)))))
