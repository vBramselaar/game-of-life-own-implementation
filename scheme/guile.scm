#!/usr/bin/env -S guile --r7rs -e entrée -s
!#

(add-to-load-path (dirname (current-filename)))

(use-modules (game-of-life gol)
             (ice-9 threads))

(define (compose-looper n proc)
  (lambda (arg)
    (let loop ((current-arg arg)
               (left n))
      (usleep 50000)
      (if (and (> left 0) (not (null? current-arg)))
          (loop (proc current-arg) (- left 1))
          current-arg))))

(define (entrée args)
  (set! *random-state* (random-state-from-platform))

  (let* ((world (make-world 100 50))
         ;;(world-with-living (add-glider world 1 1))
         (world-with-living (add-random-living world random 20))
         (evolve-and-visualize (compose visualize-in-terminal evolve))
         (simulate (compose-looper 1000 evolve-and-visualize)))
    (simulate world-with-living)))
