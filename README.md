# Game of Life own implementation
Checkout the `feature/glfw` branch to visualize the Game of Life in
OpenGL. There is also a version done with
[Raylib](https://www.raylib.com/), see the `feature/raylib` branch.

## Source
This little program is based on [Conway's Game of Life](https://en.wikipedia.org/w/index.php?title=Conway%27s_Game_of_Life&oldid=922107322)

Video explanation: https://www.youtube.com/watch?v=E8kUJL04ELA

Other people's implementations: https://rosettacode.org/wiki/Conway%27s_Game_of_Life

## Usage
**Run with:** ./program [Width] [Height] [Percentage]

"Percentage" is how much chance a cell has to be alive at the start. This is used
to randomly place some living cells. 20% is pretty good for a normal simulation.
