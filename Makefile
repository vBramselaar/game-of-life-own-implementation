CXX:=g++
CC:=gcc
CXXFLAGS:=-Wall -Wextra -pedantic -std=c++17 -DTHREAD_MODE
CCFLAGS:=-Wall -Wextra
RELEASEF:=-O3
DEBUGF:=-ggdb -O0 -DDEBUG_MODE
LDFLAGS:=-lpthread

CPPFLAGS:=-I./src

SRC:=$(wildcard src/*.cpp)
OBJ:=$(patsubst %.cpp,%.o, $(SRC))

PRG:=game-of-life

.PHONY:all clean

all:debug

release:CXXFLAGS += $(RELEASEF)
release:$(PRG)
	@echo "Release build"

debug:CXXFLAGS += $(DEBUGF) 
debug:$(PRG)
	@echo "Debug build"

clang_complete:
	@echo $(CPPFLAGS) | tr " " "\n" > .clang_complete
	@echo ".clang_complete generated"

$(PRG):$(OBJ)
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) $^ -o $@ $(LDFLAGS)

%.o:%.cpp
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) -c $< -o $@ $(LDFLAGS)

%.o:%.c
	$(CC) $(CCFLAGS) $(CPPFLAGS) -c $< -o $@ $(LDFLAGS)

clean:
	rm -rf $(OBJ)
	rm -rf $(PRG)
